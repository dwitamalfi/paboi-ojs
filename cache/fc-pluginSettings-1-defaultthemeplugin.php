<?php return array (
  'baseColour' => '#EEF7F1',
  'enabled' => true,
  'showDescriptionInJournalIndex' => 'true',
  'typography' => 'notoSans',
  'useHomepageImageAsHeader' => 'false',
);